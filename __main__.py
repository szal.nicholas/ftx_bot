import dash
from dash.dependencies import Output, Input
from dash import dcc
from dash import html
import plotly
import plotly.graph_objs as go
from collections import deque
from numpy import NaN
from bull_bear import Cryptobot
from get_price import GetPrice
from save_to_csv import DF
from math import floor,ceil

print(type)
class LiveGraph:
    """ Discription: Running graph callback is innitiated every 1000m/s, this triggers token price to be pulled 
    and evaluated.
        
    Attributes: 
    _app: type = 
    _X: lst = list with a max length. when new value gets added the end value gets released.
    _Y: lst = list with a max length. when new value gets added the end value gets released.
    BASELINE: float = max price during bull run / min price during bear run

    Return: Running graph displayed on localhost:8081
    """  
    GetPrice.connect_2_rest_api()
    GetPrice.pull_price()
    TREND_SWITCH = 0.0075 #percentage value for Threshold window
    UPDATE_INTER = 2 # amount of time in sec between HTTP requests to FTX Rest API
    COLOR='#3cb371' #rgb green
    y_min=GetPrice.price
    y_max=GetPrice.price

    #running graph
    _X = deque(maxlen = 60) #2min window
    _X.append(0)
    _Y = deque(maxlen = 60) #2min window
    _Y.append(GetPrice.price)
    #accumated graph (no implimented yet)
    _Xx=[]
    _Yy=[]
    
    _app = dash.Dash(__name__) #create dash instance. used for layout and callbacks.

    def __init__(self):

        self._app.layout = html.Div(
        [
            dcc.Graph(id = 'live-graph', animate = True),
            # dcc.Graph(id='accum-graph', animate = True), 
            dcc.Interval(id = 'graph-update', interval = self.UPDATE_INTER *1000, n_intervals = 0,),])

        if self._app is not None and hasattr(self, "callbacks"):
            self.callbacks(self._app)

    def callbacks(self, _app):
        @_app.callback(
            Output("live-graph", "figure"),
            Input(component_id="graph-update", component_property="n_intervals"))
            
        def update_graph_scatter(n):
         
            if Cryptobot.x == True: #BULL RUN
                y=GetPrice.pull_price()
                Cryptobot.bull_run(y, Cryptobot.BASELINE, __class__.TREND_SWITCH)
                self._X.append(self._X[-1]+self.UPDATE_INTER/60)
                self._Y.append(y)
                if self.COLOR != '#3cb371':#rgb green
                    self.COLOR = '#3cb371'
            
            if Cryptobot.x == False: #BEAR RUN
                y=GetPrice.pull_price()
                Cryptobot.bear_run(y, Cryptobot.BASELINE, __class__.TREND_SWITCH)
                self._X.append(self._X[-1]+self.UPDATE_INTER/60)
                self._Y.append(y)
                if self.COLOR != '#ffbf00': #rgb amber
                    self.COLOR = '#ffbf00'

            if y > self.y_max : self.y_max = y
            if y < self.y_min : self.y_min = y

            data = plotly.graph_objs.Scatter(
                x=list(self._X), y=list(self._Y), name="Scatter", mode="lines+markers", 
                line_color=self.COLOR, 
                )

            return {
                "data": [data],
                "layout": go.Layout(
                    xaxis=dict(range=[min(self._X), max(self._X)]),
                    yaxis=dict(range=[floor(self.y_min), ceil(self.y_max)]),
                ),
            }

    def start(self):
        self._app.run_server(port=8081, dev_tools_silence_routes_logging=True)

if __name__ == '__main__':
    DF.create_csv()
    live_plotter = LiveGraph()
    live_plotter.start()