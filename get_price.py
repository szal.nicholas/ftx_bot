import requests
import time

class GetPrice:
    """ Discription: sends request to FTX rest API via HTTP request

    Return: price of one token in USD
    """   
    @classmethod
    def connect_2_rest_api(cls):
        endpoint_url = 'https://ftx.com/api/markets'
        base_currency = 'SOL' 
        quote_currency = 'USD'
        cls.request_url = f'{endpoint_url}/{base_currency}/{quote_currency}'

    @classmethod
    def pull_price(cls):
        market = requests.get(cls.request_url).json()
        cls.price = market['result']['price'] #['ask']
        # print(f"{base_currency}/{quote_currency} asking price = {price}")
        cls.dt_now = int(time.time())
        return float(cls.price)


# if __name__=='__main__':
#     GetPrice.connect_2_rest_api()
#     GetPrice.pull_price()
#     print(GetPrice.price)
