from save_to_csv import DF
from colors import bcolors
import numpy

class Cryptobot:
    """ Discription: trading logic based on bull or bear condition
        
    Attributes: 
    price: float = price pulled from FTX server via rest API
    BASELINE: float = max price during bull run / min price during bear run

    Return: Bull/Bear condition as True/False
    """   
    BASELINE = 100000 #innitial baseline (needs to be alot higher than expected price)
    x=False #start with Bear Trend

    @classmethod
    def bull_run(cls, price:float, Max:int, percentage:float) ->bool: 
        if price >= Max: # this increases baseline and holds current Position.
            Max = price 
            cls.percentage_change_from_baseline = price/cls.BASELINE-1 #calculates percentage change from previous baseline
            cls.BASELINE = Max # updates baseline
            print(f"{bcolors.OKGREEN}Bull{bcolors.ENDC} run, price: {price:.2f}, baseline: {Cryptobot.BASELINE:.2f},  {Cryptobot.percentage_change_from_baseline:.4f}%")
            DF.update_df(run='Bull', current_price=price, baseline=cls.BASELINE, percentage=cls.percentage_change_from_baseline, buy=numpy.NaN, sell=numpy.NaN)
            return True
        else: 
            if price < Max: # price below baseline
                if price < Max-Max*percentage: #check if price dips outside of Threshold window
                    cls.percentage_change_from_baseline = price/cls.BASELINE-1 #calculates percentage change from previous baseline
                    Min = price #price went below Threshold window, current price set as Min
                    print('----------> BEAR RUN <> SELL SELL SELL <-----------')
                    cls.BASELINE = Min #updates baseline
                    cls.x=False
                    print(f"{bcolors.WARNING}Bear{bcolors.ENDC} run, price: {price:.2f}, baseline: {Cryptobot.BASELINE:.2f}, {Cryptobot.percentage_change_from_baseline:.4f}%")
                    DF.update_df(run='Bear', current_price=price, baseline=cls.BASELINE, percentage=cls.percentage_change_from_baseline, buy=numpy.NaN, sell=price)
                    return False ########################SELL and switch to BEAR RUN############################
                else:
                    cls.percentage_change_from_baseline = price/cls.BASELINE-1 #calculates percentage change from previous baseline
                    cls.BASELINE = Max 
                    print(f"{bcolors.OKGREEN}Bull{bcolors.ENDC} run, price: {price:.2f}, baseline: {Cryptobot.BASELINE:.2f},  {Cryptobot.percentage_change_from_baseline:.4f}%")
                    DF.update_df(run='Bull', current_price=price, baseline=cls.BASELINE, percentage=cls.percentage_change_from_baseline, buy=numpy.NaN, sell=numpy.NaN)
                    return True #price didn't go below Threshold window, bull run continues
                
    @classmethod
    def bear_run(cls, price:float, Min:int, percentage:float) -> bool:
        if price <= Min: # this decreases baseline and holds current Position.
            Min = price
            cls.percentage_change_from_baseline = price/cls.BASELINE-1 #calculates percentage change from previous baseline
            cls.BASELINE = Min #updates baseline
            print(f"{bcolors.WARNING}Bear{bcolors.ENDC} run, price: {price:.2f}, baseline: {Cryptobot.BASELINE:.2f}, {Cryptobot.percentage_change_from_baseline:.4f}%")
            DF.update_df(run='Bear', current_price=price, baseline=cls.BASELINE, percentage=cls.percentage_change_from_baseline, buy=numpy.NaN, sell=numpy.NaN)
            return False
        else:
            if price > Min:
                if price > Min+price*percentage: #check price goes above Threshold window
                    Max = price 
                    print('-----------> BULL RUN <> BUY BUY BUY <-----------')
                    cls.percentage_change_from_baseline = price/cls.BASELINE-1 #calculates percentage change from previous baseline
                    cls.BASELINE = Max #update baseline
                    cls.x=True
                    print(f"{bcolors.OKGREEN}Bull{bcolors.ENDC} run, price: {price:.2f}, baseline: {cls.BASELINE:.2f},  {cls.percentage_change_from_baseline:.4f}%")
                    DF.update_df(run='Bull', current_price=price, baseline=cls.BASELINE, percentage=cls.percentage_change_from_baseline, buy=price, sell=numpy.NaN)
                    return True #########################BUY and switch to BULL RUN############################
                    
                else:
                    cls.percentage_change_from_baseline = price/cls.BASELINE-1 #calculates percentage change from previous baseline
                    cls.BASELINE = Min
                    print(f"{bcolors.WARNING}Bear{bcolors.ENDC} run, price: {price:.2f}, baseline: {Cryptobot.BASELINE:.2f}, {Cryptobot.percentage_change_from_baseline:.4f}%")
                    DF.update_df(run='Bear', current_price=price, baseline=cls.BASELINE, percentage=cls.percentage_change_from_baseline, buy=numpy.NaN, sell=numpy.NaN)
                    return False #price didnt go above percentage window / bear run continues

