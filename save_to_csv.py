import pandas as pd
from datetime import datetime
import os
from pathlib import Path

pd.options.display.float_format = '{:,.4f}'.format

# FILEPATH='/Users/obiwan/Desktop/Python/Cryptobot'

class DF:
    """ Discription: constructs dataframe
        
    Attributes: 
    run: str = "Bull" or "Bear" 
    current_price: float = last pulled price
    baseline: float = current baseline
    percentage: float = current deviation from baseline
    buy: float = buy price during bear-to-bull conditon change
    sell: float = sell price during bull-to-bear condition change

    Return: creates and appends csv file
    """   
    FILEPATH=Path(__file__).resolve().parent

    @classmethod
    def create_csv(cls):
        cls.path= os.path.join(cls.FILEPATH, datetime.now().strftime('%Y.%m.%d__%H;%M')+'.csv')
        df = pd.DataFrame(columns=['Date', 'Run', 'Current_Price', 'Baseline_Price', 'Percentage_Diff', 'Buy', 'Sell'])
        df.to_csv(cls.path, header=True, index=None, sep=',', mode='a')

    @classmethod
    def update_df(cls, run, current_price, baseline, percentage, buy, sell):
        df = pd.DataFrame([(datetime.now().strftime('%H:%M:%S'), run, current_price, baseline, percentage, buy, sell)],
        columns=['Date', 'Run', 'Current_Price', 'Baseline_Price', 'Percentage_Diff', 'Buy', 'Sell'])
        df=df.round(decimals=4)
        df.to_csv(cls.path, header=None, index=None, sep=',', mode='a')
